using root.decorators;
using root.toppings;

namespace root
{
    class FactoryZA : IngredientFactory
    {
        Ingredient ingredient;
        public FactoryZA(Ingredient ingredient)
        {
            this.ingredient = ingredient;
        }
        public Poultry CreatePoultry()
        {
            return new Chicken(ingredient);
        }
        public Meat CreateMeat()
        {
            return new Mutton(ingredient);
        }
        public Vegetation CreateVegetation()
        {
            return new Samp(ingredient);
        }
        public Cheese CreateCheese()
        {
            return new Cheddar(ingredient);
        }
        public toppings.Guacamole CreateGuacamole()
        {
            return new SlicedAvocados(ingredient);
        }
        public Rice CreateRice()
        {
            return new Jasmin(ingredient);
        }
        public Beans CreateBeans()
        {
            return new RefriedBeans(ingredient);
        }
        public CreamCheese CreateCream()
        {
            return new SmoothCream(ingredient);
        }
        public Tomato CreateTomato()
        {
            return new Relish(ingredient);
        }
        public Chili CreateChili()
        {
            return new Jalapeno(ingredient);
        }
    }
}
