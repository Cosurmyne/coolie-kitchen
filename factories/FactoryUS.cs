using root.decorators;
using root.toppings;

namespace root
{
    class FactoryUS : IngredientFactory
    {
        Ingredient ingredient;
        public FactoryUS(Ingredient ingredient)
        {
            this.ingredient = ingredient;
        }
        public Poultry CreatePoultry()
	{
		return new Turkey(ingredient);
	}
        public Meat CreateMeat()
	{
		return new Beef(ingredient);
	}
        public Vegetation CreateVegetation()
	{
		return new Chickpeas(ingredient);
	}
        public Cheese CreateCheese()
	{
		return new PepperJack(ingredient);
	}
        public toppings.Guacamole CreateGuacamole()
	{
		return new decorators.Guacamole(ingredient);
	}
        public Rice CreateRice()
	{
		return new Basmati(ingredient);
	}
        public Beans CreateBeans()
	{
		return new BlackBeans(ingredient);
	}
        public CreamCheese CreateCream()
	{
		return new ChunkyCream(ingredient);
	}
        public Tomato CreateTomato()
	{
		return new Salsa(ingredient);
	}
        public Chili CreateChili()
	{
		return new Habanero(ingredient);
	}
    }
}
