using root.toppings;

namespace root
{
    interface IngredientFactory
    {
        Poultry CreatePoultry();
        Meat CreateMeat();
        Vegetation CreateVegetation();
        Cheese CreateCheese();
        Guacamole CreateGuacamole();
        Rice CreateRice();
        Beans CreateBeans();
        CreamCheese CreateCream();
        Tomato CreateTomato();
        Chili CreateChili();
    }
}
