namespace root
{
	abstract class Ingredient
	{
		public Kitchen Kitchen { get; set; }
		public abstract string GetName();
		public abstract string GetDescription();
		public abstract double GetCost();
	}
}
