//Ĥ
using System;

namespace root
{
    class ObserverKitchen : IObserver, IDisplay
    {
        private Observable observable;
        private Kitchen kitchen;

        public ObserverKitchen(Observable observable)
        {
            this.observable = observable;
            observable.AddObserver(this);
        }
        public void Update(Observable obs, object arg)
        {
            if (obs is Subject)
            {
                Subject subject = (Subject)obs;
                this.kitchen = subject.GetKitchen();
                Display();
            }
        }
        public void Display()
        {
	    Console.Clear();
            Console.WriteLine("Order placed in the {0} Kitchen", this.kitchen.GetDescription());
        }
    }
}
