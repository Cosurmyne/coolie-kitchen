using System;
using System.Collections.Generic;

namespace root
{
    static class UI
    {
        private static readonly Kitchen[] KITCHEN = { new KicthenUS(), new KicthenZA() };
        private static readonly string[] INGREDIENT = { "Burrito", "Taco" };
        private static readonly List<string> current = new List<string>();	// Current Toppings in customer Ingredient
        private delegate void Update();

        // Prompt and Get current Kitchen
        public static Kitchen Kitchen()
        {
            return KITCHEN[Prompt("Select Kitchen", new string[] { KITCHEN[0].GetDescription(), KITCHEN[1].GetDescription() }, new Update(() => { }))];
        }

        // Prompt and Get current Ingredient
        public static Ingredient Ingredient(Kitchen kitchen)
        {
            return kitchen.PlaceOrder(INGREDIENT[Prompt("Select Ingredient", INGREDIENT, new Update(() => { }))]);
        }

        // Verify if Topping will be required for the current order.
        public static bool IsTopping(Subject fx)
        {
            return Prompt("Add Toppings?", new string[] { "No", "Yes" }, new Update(fx.DataChanged)) == 1;
        }

        // Prompt and Get Toppings for the current order.
        public static void GetTopping(Subject subject)
        {
            // Reload list of Toppings for user to select from, in the Factory Client (Kitchen instances in this solution).
            subject.GetKitchen().PrepareToppings(subject.GetIngredient(), current);
	    // Make the up-to-date list available for manipulation to this Method.
            List<Ingredient> toppings = subject.GetKitchen().toppings;
	    // Convert list to an array of strings.
            string[] fillings = new string[toppings.Count];
            for (byte i = 0; i < fillings.Length; i++)
            {
                string[] temp = toppings[i].GetDescription().Split(',');
                fillings[i] = temp[temp.Length - 1].Trim();
            }
	    // Use array to generate user prompt string, and get response.
            byte selection = (byte)Prompt("Make Selection", fillings, new Update(() => { }));
	    // Decorated current Ingredient with the user selected topping.
            Ingredient decoratorated = toppings[selection];

	    // Update running list of selected toppings
            current.Add(fillings[selection]);
	    // Remind 
            decoratorated.Kitchen = subject.GetKitchen();
	    // Update Subject with the current of the Ingredient, and Notify Observers
            subject.TakeOrder(decoratorated);
        }

        static int Prompt(string prompt, string[] options, Update update)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                update();	// incase you wish to notify observers
                Console.WriteLine("{0}", prompt);
                Console.WriteLine("==============");
                for (byte i = 0; i < options.Length; i++) Console.WriteLine("- {0} :: {1}", i, options[i]);
                Console.WriteLine();
                Console.Write("selection >> ");

                if (!IsInt(Console.ReadLine(), ref val)) continue;
                if (val >= 0 && val < options.Length) break;
            } while (true);

            return val;
        }
        static bool IsInt(string input, ref Int32 val)
        {
            try
            {
                val = int.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
    }
}
