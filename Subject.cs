namespace root
{
    class Subject : Observable
    {
        private Kitchen kitchen;
        private Ingredient ingredient;

        public void DataChanged()
        {
            SetChanged();
            NotifyObservers();
        }
        public void TakeOrder(Ingredient ingredient)
        {
            this.kitchen = ingredient.Kitchen;
            this.ingredient = ingredient;
            DataChanged();
        }
        public Kitchen GetKitchen()
        {
            return this.kitchen;
        }
        public Ingredient GetIngredient()
        {
            return this.ingredient;
        }
    }
}
