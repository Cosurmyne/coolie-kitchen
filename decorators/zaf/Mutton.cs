using root.toppings;
namespace root.decorators
{
	class Mutton : Meat
	{
		Ingredient ingredient;
		public Mutton(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Mutton";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Mutton";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 22;
		}
	}	
}
