using root.toppings;
namespace root.decorators
{
	class SmoothCream : CreamCheese
	{
		Ingredient ingredient;
		public SmoothCream(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Smooth Cream Cheese";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Smooth Cream Cheese";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 14;
		}
	}	
}
