using root.toppings;
namespace root.decorators
{
	class Samp : Vegetation
	{
		Ingredient ingredient;
		public Samp(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Samp";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Samp";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 10;
		}
	}	
}
