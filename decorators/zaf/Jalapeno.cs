using root.toppings;
namespace root.decorators
{
	class Jalapeno : Chili
	{
		Ingredient ingredient;
		public Jalapeno(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Jalapeno Chilies";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Jalapeno Chilies";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 8;
		}
	}	
}
