using root.toppings;
namespace root.decorators
{
	class Chicken : Poultry
	{
		Ingredient ingredient;
		public Chicken(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Chicken";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Chicken";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 15;
		}
	}	
}
