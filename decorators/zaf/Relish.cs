using root.toppings;
namespace root.decorators
{
	class Relish : Tomato
	{
		Ingredient ingredient;
		public Relish(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Relish";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Relish";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 5;
		}
	}	
}
