namespace root.decorators
{
	class SlicedAvocados : toppings.Guacamole
	{
		Ingredient ingredient;
		public SlicedAvocados(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Sliced Avocados";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Sliced Avocados";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 15;
		}
	}	
}
