using root.toppings;
namespace root.decorators
{
	class Jasmin : Rice
	{
		Ingredient ingredient;
		public Jasmin(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Jasmin Rice";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Jasmin Rice";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 4;
		}
	}	
}
