using root.toppings;
namespace root.decorators
{
	class Cheddar : Cheese
	{
		Ingredient ingredient;
		public Cheddar(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Cheddar Cheese";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Cheddar Cheese";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 6;
		}
	}	
}
