using root.toppings;
namespace root.decorators
{
	class RefriedBeans : Beans
	{
		Ingredient ingredient;
		public RefriedBeans(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Refried Beans";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Refried Beans";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 6;
		}
	}	
}
