namespace root.toppings
{
    abstract class Tomato : IngredientDecorator
    {
		Ingredient ingredient;
		public Tomato(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
