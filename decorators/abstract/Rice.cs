namespace root.toppings
{
    abstract class Rice : IngredientDecorator
    {
		Ingredient ingredient;
		public Rice(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
