namespace root.toppings
{
    abstract class Vegetation : IngredientDecorator
    {
		Ingredient ingredient;
		public Vegetation(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
