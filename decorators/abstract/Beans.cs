namespace root.toppings
{
    abstract class Beans  : IngredientDecorator
    {
		Ingredient ingredient;
		public Beans(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
