namespace root.toppings
{
    abstract class Chili : IngredientDecorator
    {
		Ingredient ingredient;
		public Chili(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
