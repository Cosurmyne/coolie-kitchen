namespace root.toppings
{
    abstract class Meat : IngredientDecorator
    {
		Ingredient ingredient;
		public Meat(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
