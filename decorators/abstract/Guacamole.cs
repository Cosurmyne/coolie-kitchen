namespace root.toppings
{
    abstract class Guacamole : IngredientDecorator
    {
		Ingredient ingredient;
		public Guacamole(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
