namespace root
{
	abstract class IngredientDecorator :Ingredient
	{
		Ingredient ingredient = null;
		public IngredientDecorator(Ingredient ingredient)
		{
			this.ingredient = ingredient;
		}
	}
}
