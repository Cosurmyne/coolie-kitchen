using root.toppings;
namespace root.decorators
{
    class ChunkyCream : CreamCheese
    {
        Ingredient ingredient;
        public ChunkyCream(Ingredient ingredient) : base(ingredient)
        {
            this.ingredient = ingredient;
        }
        public override string GetName()
        {
            return "Chunky Cream Cheese";
        }
        public override string GetDescription()
        {
            return ingredient.GetDescription() + ", Chunky Cream Cheese";
        }
        public override double GetCost()
        {
            return ingredient.GetCost() + 12;
        }
    }
}
