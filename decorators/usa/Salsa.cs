using root.toppings;
namespace root.decorators
{
	class Salsa : Tomato
	{
		Ingredient ingredient;
		public Salsa(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Salsa";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Salsa";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 5;
		}
	}	
}
