namespace root.decorators
{
	class Guacamole: toppings.Guacamole
	{
		Ingredient ingredient;
		public Guacamole(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Guacamole";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Guacamole";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 10;
		}
	}	
}
