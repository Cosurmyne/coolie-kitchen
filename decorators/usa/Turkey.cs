using root.toppings;

namespace root.decorators
{
	class Turkey : Poultry
	{
		Ingredient ingredient;
		public Turkey(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Turkey";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Turkey";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 15;
		}
	}	
}
