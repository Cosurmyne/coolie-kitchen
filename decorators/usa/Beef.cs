using root.toppings;
namespace root.decorators
{
	class Beef : Meat
	{
		Ingredient ingredient;
		public Beef(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Beef";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Beef";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 20;
		}
	}	
}
