using root.toppings;
namespace root.decorators
{
	class Chickpeas : Vegetation
	{
		Ingredient ingredient;
		public Chickpeas(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Chickpeas";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Chickpeas";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 10;
		}
	}	
}
