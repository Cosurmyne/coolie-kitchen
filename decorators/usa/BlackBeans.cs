using root.toppings;
namespace root.decorators
{
	class BlackBeans : Beans
	{
		Ingredient ingredient;
		public BlackBeans(Ingredient ingredient) :base(ingredient)

		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Black Beans";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Black Beans";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 5;
		}
	}	
}
