using root.toppings;
namespace root.decorators
{
	class Basmati : Rice
	{
		Ingredient ingredient;
		public Basmati(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Basmati Rice";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Basmati Rice";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 3;
		}
	}	
}
