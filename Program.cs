﻿namespace root
{
    class Program
    {
        static void Main(string[] args)
        {
            Subject subject = new Subject();	// Represents aplication user. Could be Cashier, or Customer.

            ObserverKitchen kitchen = new ObserverKitchen(subject);	// Responsible for displaying the currently selected Kitchen
            ObserverIngredient ingredient = new ObserverIngredient(subject);	// Responsible for displaying the currently delected Product

            subject.TakeOrder(UI.Ingredient(UI.Kitchen()));	// Notify Observers of User selections.

	    //	Find out if user is interested to have toppings on the ingredient
	    while(UI.IsTopping(subject))
	    {
		    UI.GetTopping(subject);	// Method includes Toppings list Prompt, and functionality to make selection.
	    }
	    subject.DataChanged();	// Update final display.
        }
    }
}
