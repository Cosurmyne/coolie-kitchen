using System.Collections.Generic;

namespace root
{
    abstract class Kitchen
    {

        public Ingredient PlaceOrder(string type)
        {
            Ingredient ingredient;

            ingredient = FactoryMethod(type);
            ingredient.Kitchen = this;
            return ingredient;
        }
        public List<Ingredient> toppings { get; set; }
        public abstract Ingredient FactoryMethod(string type);
        public abstract string GetDescription();
        public abstract void PrepareToppings(Ingredient ingredient, List<string> current);
    }
}
