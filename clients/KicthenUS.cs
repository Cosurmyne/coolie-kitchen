using System.Collections.Generic;

namespace root
{
    class KicthenUS : Kitchen
    {
        public KicthenUS()
        {
            this.toppings = new List<Ingredient>();
        }
        public override string GetDescription()
        {
            return "United States";
        }
        public override Ingredient FactoryMethod(string type)
        {
            Ingredient ingredient = null;
            switch (type.Trim().ToLower())
            {
                case "burrito":
                    ingredient = new Burrito();
                    break;
                case "taco":
                    ingredient = new Taco();
                    break;
            }
            return ingredient;
        }
        public override void PrepareToppings(Ingredient ingredient, List<string> current)
        {
            IngredientFactory factory = new FactoryUS(ingredient);
            toppings.Clear();
            if (!current.Contains(factory.CreatePoultry().GetName())) toppings.Add(factory.CreatePoultry());
            if (!current.Contains(factory.CreateMeat().GetName())) toppings.Add(factory.CreateMeat());
            if (!current.Contains(factory.CreateVegetation().GetName())) toppings.Add(factory.CreateVegetation());
            if (!current.Contains(factory.CreateCheese().GetName())) toppings.Add(factory.CreateCheese());
            if (!current.Contains(factory.CreateGuacamole().GetName())) toppings.Add(factory.CreateGuacamole());
            if (!current.Contains(factory.CreateRice().GetName())) toppings.Add(factory.CreateRice());
            if (!current.Contains(factory.CreateBeans().GetName())) toppings.Add(factory.CreateBeans());
            if (!current.Contains(factory.CreateCheese().GetName())) toppings.Add(factory.CreateCheese());
            if (!current.Contains(factory.CreateTomato().GetName())) toppings.Add(factory.CreateTomato());
            if (!current.Contains(factory.CreateChili().GetName())) toppings.Add(factory.CreateChili());
        }
    }
}
